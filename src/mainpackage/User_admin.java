
package mainpackage;

public class User_admin {
    private int ID;
    private String username,password;
    User_admin(int ID,String username,String password){
        this.ID = ID;
        this.username = username;
        this.password = password;
    }
    
    public int getid(){
        return ID;
    }
    public String getusername(){
        return username;
    }
    public String getpassword(){
        return password;
    }
}
