package mainpackage;

public class User {
    private int ID;
    private String fname,lname,mname,dob,fathname,mothname,address;
    private byte picture[];
    User(int ID,String fname,String lname,String mname,String dob,String fathname,String mothname,String address,byte[] image){
        this.ID = ID;
        this.fname = fname;
        this.lname = lname;
        this.mname = mname;
        this.dob = dob;
        this.fathname = fathname;
        this.mothname = mothname;
        this.address = address;
        this.picture = image;
    }
    
    public int getid(){
        return ID;
    }
    public String getfname(){
        return fname;
    }
    public String getlname(){
        return lname;
    }
    public String getmname(){
        return mname;
    }
    public String getdob(){
        return dob;
    }
    public String getfathname(){
        return fathname;
    }
    public String getmothname(){
        return mothname;
    }
    public String getaddress(){
        return address;
    }
    public byte[] getImage(){
        return picture;
    }
}
